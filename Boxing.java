import java.util.ArrayList;

/**
* Boxing class
*
* example of boxing (autobox and unbox)
*
* COMPILE
*
* javac Boxing.java
*
* RUN EXAMPLE
*
* java Boxing 5
* 
* EXAMPLE OUTPUT
* 
* integer 3
* integer 5
* integer 7
* integer 9
* integer 11
* 
* BOXING
* 
* add a value to some other object
* 
* UNBOXING
* 
* remove a value from an object and use it
* 
*/
class Boxing{
    public static void main(String args[]){
        ArrayList<Integer> odds; // odd numbers
        int count=5; // add 5 numbers
        int j;
        int n;

        odds=new ArrayList<Integer>();

        if(args.length != 1){
            return;
        }

        count=Integer.parseInt(args[0]);

        for(j=1;j<=count;j++){
            odds.add(Integer.valueOf((2*j)+1)); // autobox integers inside Integer object
        }

        for(j=0;j<count;j++){
            n=odds.get(j); // unbox an integer
            System.out.println("integer "+n);
        }
    }
}
